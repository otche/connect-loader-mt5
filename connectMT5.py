#!/usr/bin/python3
import MetaTrader5._core as mt5
import MetaTrader5 as mt5root
from datetime import datetime
from log import Logging
from utils import switch_TIMEFRAME


def connect(HOST, PORT, COMPTE_ID, COMPTE_PSW, BROCKER_SERVER):
    """ @description:
    @param HOST: 
    @param PORT: 
    @param COMPTE_ID: 
    @param COMPTE_PSW: 
    @param BROCKER_SERVER: 
    """
    logging = Logging.getLogging()
    logging.debug("\n")
    logging.debug("========== START connect() ==========")
    if not mt5.initialize(login=COMPTE_ID, 
        server=BROCKER_SERVER,
        password=COMPTE_PSW) :
        quit()
        ERROR_MSG = "FAIL TO CONNECT INITIALIZE, ERROR CODE ==> "+mt5.last_error()
        logging.error(ERROR_MSG)
        raise ERROR_MSG
    logging.debug("========== END connect() ==========\n")
    logging.info("CONNECTION SUCCESS")
    logging.info(mt5.account_info())
    return mt5

def shutdown() :
    """ @description:
    """
    logging = Logging.getLogging()
    logging.debug("\n")
    logging.debug("========== START shutdown() ==========")
    logging = Logging.getLogging()
    mt5.shutdown()
    logging.info("SHUT DOWN MT5 SUCCESS")
    logging.debug("========== END shutdown() ==========\n")


def getDataRangeMT5(market, timeframe, from_datetime, to_datetime) :
    """ 
    @description:
    @param market: 
    @param timeframe: 
    @param from_datetime: 
    @param to_datetime: 
    """
    logging = Logging.getLogging()
    logging.debug("\n")
    logging.debug("========== START getDataRangeMT5() ==========")
    rates = mt5.copy_rates_range(market, switch_TIMEFRAME(timeframe), from_datetime, to_datetime)
    logging.info("result length for {market} with {timeframe} from {from_datetime} to {to_datetime} is {length} ".format(
        market=market, timeframe=timeframe, from_datetime=from_datetime, to_datetime= to_datetime, length=len(rates)))
    for rate in rates:
        yield {
            'timestamp': int(rate[0]),
            'open': float(rate[1]),
            'high': float(rate[2]),
            'low': float(rate[3]),
            'close': float(rate[4]),
            'tick_volume': int(rate[5]),
            'spread': int(rate[6]), 
            'real_volume': int(rate[7])
        }
        #logging.info(datetime.fromtimestamp(rate[0]))
    logging.debug("========== END getDataRangeMT5() ==========\n")
    