class ArgEntryExeption(Exception):
    """
    """
    def __init__(self, entry_arg):
        self.__ERROR_MSG = "Arguments errors fromat ==> {enty_arg} must be KEY=VAL".format(
                        enty_arg = entry_arg)
        Exception.__init__(self, self.__ERROR_MSG)


class DuplicateEntryException(Exception):
    """
    """
    def __init__(self, elmt_key):
        self.__ERROR_MSG = "Duplicate argument {elmt_key}".format(elmt_key = elmt_key)
        Exception.__init__(self, self.__ERROR_MSG)

def getArgs(argv):
    """ 
    @description:
    @param argv: 
    """
    rest = {}
    for arg in argv:
        enty_arg = arg.split("=")
        if(len(enty_arg) != 2):
            raise ArgEntryExeption(arg)

        elmt_key = enty_arg[0].strip()
        if(elmt_key in rest) :
            raise DuplicateEntryException(elmt_key)
        rest[elmt_key] = enty_arg[1].strip()
    return rest


def checkKey(key, args):
    """ 
    @description:
    @param argv: 
    """
    return True if (key in args and args[key] != None ) else False

