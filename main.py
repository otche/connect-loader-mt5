#!/usr/bin/python3
from os import environ
from datetime import datetime
from pytz import timezone
import sys
import re
from connectMT5 import connect, shutdown, getDataRangeMT5,  switch_TIMEFRAME
from utils import parseDatetimeStr, objToDatatime, getTimeframeIntervalUnit,buildRangePageIntervals, DEFAULT_TIMEZONE, switch_TIMEFRAME
from utilsBeforLog import  getArgs, checkKey
from log import Logging
import json
import time


FROM_DATETIME_KEY = "FROM_DATETIME"
TO_DATETIME_KEY = "TO_DATETIME"
TIMEZONE_KEY = "TIMEZONE"
TIMEFRAME_KEY = "TIMEFRAME"
MARKET_KEY = "MARKET"
REALTIME_MODE_KEY = "REALTIME_MODE"



def dataHistoryLoad(from_datetime, to_datetime, maxResultPageSize, market, timeframe, tz):
    """ 
    @description:
    @param from_datetime: 
    @param to_datetime: 
    @param maxResultPageSize: 
    @param timeframe: 
    @param tz: 
    """
    iter = buildRangePageIntervals(from_datetime, to_datetime, 
            maxResultPageSize = maxResultPageSize , timeframe= timeframe,tz= tz)
    lst = list(iter)
    for index in range(0, len(lst)-1):
        _data = getDataRangeMT5(market, timeframe, lst[index], lst[index+1])
        list_data = list(_data)
        print(json.dumps(list_data))


def realTimeDataLoad(lastLoadTime, timeframe, market,  tz) :
    """ 
    @description:
    @param lastLoadTime: 
    @param interval: 
    """
    logging = Logging.getLogging()
    interval = getTimeframeIntervalUnit(timeframe)
    _lastLoadTime = lastLoadTime
    while True :
        nowTime =  datetime.now(tz = timezone(tz))
        logging.info("load data in interval => {interval}".format(interval = interval))
        logging.info("load data from  => {fromDate}".format(fromDate = _lastLoadTime))
        logging.info("to data from  => {nowTime}".format(nowTime = nowTime))
        _data = getDataRangeMT5(
            market= market, 
            timeframe=timeframe,
            from_datetime= _lastLoadTime ,
            to_datetime=nowTime)
        _lastLoadTime = nowTime
        try :
            list_data = list(_data)
            print(json.dumps(list_data))
        except Exception as e :
            logging.warning("no data in interval ")
            logging.warning("Exception ==>{e} ".format(e=e))
        time.sleep(interval)


def getDataBeginDate(market, timeframe, tz) :
    """ 
    @description:
    @param market: 
    @param timeframe: 
    """
    firstUniqueRslt = getDataRangeMT5(market, timeframe, 
                        datetime(year=2010, month=1, day= 1, tzinfo=timezone(tz)), 
                        datetime(year=2010, month=1, day= 2, tzinfo=timezone(tz)))
    uniqueRslt = list(firstUniqueRslt)
    if(len(uniqueRslt)!= 1):
        raise Exception("uncoherente lenth data of first ping result, must be 1 but result => {length} ".format(length = len(uniqueRslt)))
    for rslt in uniqueRslt :
        return datetime.fromtimestamp(rslt['timestamp'], tz= timezone(tz))
        

if __name__ == "__main__":
    try:
        logging = Logging.getLogging()
        logging.info('Program argument =>  {argv}  '.format(argv=sys.argv[1:]))
        
        args = getArgs(sys.argv[1:])
        logging.info('Parsed program argument => {args} '.format(args=args))

        connect(args["HOST"], args["PORT"], int(args["COMPTE_ID"]), args["COMPTE_PSW"], args["BROCKER_SERVER"])

        MARKET = args[MARKET_KEY] if(checkKey(MARKET_KEY, args)) else None
        
        if(MARKET == None):
            raise Exception("Must specify Market name expl =>  'GOLD.' ")

        TIMEFRAME = args[TIMEFRAME_KEY] if(checkKey(TIMEFRAME_KEY, args)) else None #""
        if(TIMEFRAME == None):
            raise Exception("Must specify Time frame name expl =>  'TIMEFRAME_MN1' ")

        if(switch_TIMEFRAME(TIMEFRAME) == None):
            raise Exception("Unreconize time frame => {timeframe} ".format(timeframe = TIMEFRAME))
        
        TIMEZONE = args[TIMEZONE_KEY] if(checkKey(TIMEZONE_KEY, args)) else DEFAULT_TIMEZONE
        if(timezone(TIMEZONE) == None) :
            raise Exception("Unreconize time zone => {tz} ".format(tz = TIMEZONE))

        FROM_DATETIME = args[FROM_DATETIME_KEY] if(checkKey(FROM_DATETIME_KEY, args)) else None
        TO_DATETIME = args[TO_DATETIME_KEY] if(checkKey(TO_DATETIME_KEY, args)) else None
        
        from_datetime = getDataBeginDate(MARKET,TIMEFRAME, TIMEZONE) if((FROM_DATETIME == None)) else objToDatatime(parseDatetimeStr(FROM_DATETIME), TIMEZONE)
        to_datetime = datetime.now(tz = timezone(TIMEZONE)) if((FROM_DATETIME == None)) else objToDatatime(parseDatetimeStr(TO_DATETIME), TIMEZONE)

        REALTIME_MODE  = (args[REALTIME_MODE_KEY]=='TRUE') if(TO_DATETIME == None and checkKey(REALTIME_MODE_KEY, args)) else True
        

        logging.debug('from datetime =>  {from_datetime}  '.format(from_datetime=from_datetime))
        logging.debug('to datetime =>  {to_datetime}  '.format(to_datetime=to_datetime))
        
        dataHistoryLoad(from_datetime, to_datetime, 100,MARKET, TIMEFRAME, TIMEZONE)
        if(REALTIME_MODE):
            logging.info("REAL TIME MODE ...")
            realTimeDataLoad(
                lastLoadTime =to_datetime, 
                timeframe= TIMEFRAME, 
                market=MARKET, 
                tz=TIMEZONE)
        
    except Exception as e:
        MSG_ERROR = "ERROR => {error}".format(error =e)
        logging.critical(MSG_ERROR)
        print(json.dumps({'error' : MSG_ERROR }))
    finally:
        shutdown()