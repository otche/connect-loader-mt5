#!/usr/bin/python3
from os import environ
from datetime import datetime
from pytz import timezone
import sys
import re
import MetaTrader5 as mt5root
from log import Logging



DEFAULT_TIMEZONE = "Etc/UTC"
MINUTE =  60#1000 * 60
HOUR = MINUTE * 60
DAY = HOUR * 24
WEEK = DAY * 7
MONTH = DAY * 30


switcher = {
    "TIMEFRAME_M1": mt5root.TIMEFRAME_M1,
    "TIMEFRAME_M2": mt5root.TIMEFRAME_M2,
    "TIMEFRAME_M3": mt5root.TIMEFRAME_M3,
    "TIMEFRAME_M4": mt5root.TIMEFRAME_M4,
    "TIMEFRAME_M5": mt5root.TIMEFRAME_M5,
    "TIMEFRAME_M6": mt5root.TIMEFRAME_M6,
    "TIMEFRAME_M10": mt5root.TIMEFRAME_M10,
    "TIMEFRAME_M12": mt5root.TIMEFRAME_M12,
    "TIMEFRAME_M15": mt5root.TIMEFRAME_M15,
    "TIMEFRAME_M20": mt5root.TIMEFRAME_M20,
    "TIMEFRAME_M30": mt5root.TIMEFRAME_M30,
    "TIMEFRAME_H1": mt5root.TIMEFRAME_H1,
    "TIMEFRAME_H2": mt5root.TIMEFRAME_H2,
    "TIMEFRAME_H3": mt5root.TIMEFRAME_H3,
    "TIMEFRAME_H4": mt5root.TIMEFRAME_H4,
    "TIMEFRAME_H6": mt5root.TIMEFRAME_H6,
    "TIMEFRAME_H8": mt5root.TIMEFRAME_H8,
    "TIMEFRAME_H12": mt5root.TIMEFRAME_H12,
    "TIMEFRAME_D1": mt5root.TIMEFRAME_D1,
    "TIMEFRAME_W1": mt5root.TIMEFRAME_W1,
    "TIMEFRAME_MN1": mt5root.TIMEFRAME_MN1
}

class DatetimeStrFormatException(Exception):
    """
    """
    def __init__(self, date_time_str, errorType=None):
        if(errorType == None) :
            self.__ERROR_MSG = "Datetime string format error => {date_time_str}".format(date_time_str=date_time_str)
            Exception.__init__(self, self.__ERROR_MSG)    
        elif(errorType == "YEAR"):
            self.__ERROR_MSG = "YEAR must be in args keys in ==> {date_time_str}  ".format(date_time_str=date_time_str)
            Exception.__init__(self, self.__ERROR_MSG)   


class UndifinedTimeframe(Exception):
    def __init__(self, timeframe):
        self.__ERROR_MSG = "undifined timeframe ==> {timeframe} ==> must be ".format(timeframe =timeframe)
        Exception.__init__(self, self.__ERROR_MSG) 



    
def parseIntArg(args, arg_key, defaultVal=0):
    """
    @description:
    @param argv: 
    @param arg_key: 
    @param defaultVal: 
    """
    return int(args[arg_key]) if (arg_key in args and args[arg_key] != None ) else defaultVal


def parseDatetimeStr(date_time_str):
    """
    @description:
    @param dateArg: 
    """
    logging = Logging.getLogging()
    logging.debug("\n")
    logging.debug("========== START parseDateArg() ==========")
    END_FUNC_MSG = "========== END parseDateArg() ==========\n"
    
    rest={}
    datetime_str_format_error = DatetimeStrFormatException(date_time_str)
    logging.debug("strat parsing ...")
    datetime = date_time_str.split("|")

    if(len(datetime) != 1 and len(datetime) != 2 ):
        raise datetime_str_format_error
    
    logging.debug("parse date ...")
    
    date = datetime[0].split("/")
    if(len(date) == 0) :
        raise DatetimeStrFormatException(date, "YEAR")
    
    if(len(date) > 3) :
        raise datetime_str_format_error
    
    try :
        rest['year'] = int(date[0])
    except Exception : 
        raise datetime_str_format_error
    
    if(len(date) != 3):
        raise datetime_str_format_error
    
    try :
        rest['month'] = int(date[1])
        rest['day'] = int(date[2])
        logging.debug("parse time ...")
    except Exception : 
        raise datetime_str_format_error
    
    if(len(datetime) == 1) :
        logging.debug(END_FUNC_MSG)
        return rest

    time = datetime[1].split(":")
    
    if(len(time) != 3):
        raise datetime_str_format_error 
    try :
        rest['hour'] = int(time[0])
        rest['minute'] = int(time[1])
        rest['second'] = int(time[2])
    except Exception : 
        raise datetime_str_format_error
    
    return rest


def objToDatatime(args, tz = DEFAULT_TIMEZONE):
    """
    @description:
    @param args: 
    """
    logging = Logging.getLogging()
    logging.debug("\n")
    logging.debug("========== START objToDatatime() ==========")
    year = parseIntArg(args, 'year')
    if(year == 0 ):
        raise DatetimeStrFormatException(args,'YEAR')
    month = parseIntArg(args, 'month', 1)
    day = parseIntArg(args, 'day', 1)
    hour = parseIntArg(args, 'hour')
    minute = parseIntArg(args, 'minute')
    second = parseIntArg(args, 'second')
    microsecond = parseIntArg(args, 'microsecond')
    tz= args['tz'] if ('tz' in args and args['tz'] != None ) else "Etc/UTC"
    logging.debug('INPUT DATETIME MENBERS ==> {year}/{month}/{day}-{hour}:{minute}:{second}:{microsecond}::{tz}'.format(
        year= year,
        month = month,
        day = day,
        hour = hour,
        minute = minute,
        second = second,
        microsecond = microsecond,
        tz = tz
    ))

    buildDataTime = datetime(
        year=year,
        month= month,
        day= day, 
        hour= hour,
        minute= minute,
        second= second,
        microsecond= microsecond,
        tzinfo=timezone(tz))
    
    logging.debug("Built datatime => {buildDataTime}".format(buildDataTime =buildDataTime))
    logging.debug("========== END objToDatatime() ==========\n")
    return buildDataTime


def switch_TIMEFRAME(timeframe):
    """ 
    @description:
    @param log_level_str: 
    """
    
    return switcher.get(timeframe)

def getTimeframeIntervalUnit(timeframe):
    """ 
    @description:
    @param timeframe: 
    """
    logging = Logging.getLogging()
    logging.debug("\n")
    logging.debug("========== START getTimeframeIntervalUnit() ==========")
    logging.debug(timeframe)
    if(switch_TIMEFRAME(timeframe) == None):
        raise UndifinedTimeframe(timeframe =timeframe)
    
    timeframeType = timeframe.split('_')[1]
    logging.debug("timeframeType => {timeframeType} ".format(timeframeType = timeframeType))
    result = None
    if(timeframeType=='MN1'): 
        result = MONTH
    elif (timeframeType=='W1'): 
        result =  WEEK
    elif (timeframeType=='D1'):
        result = DAY 
    elif(timeframeType[0]=='H'):
        result = HOUR * int(timeframeType[1:])
    elif(timeframeType[0]=='M'):
        result = MINUTE * int(timeframeType[1:])
    logging.debug('result {result}'.format(result=result))
    logging.debug("========== END getTimeframeIntervalUnit() ==========\n")
    return result
   

def buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize, timeframe, tz ):
    """ 
    @description:
    @param fromDatetime: 
    @param toDatetime: 
    @param maxResultPageSize: 
    @param timeframe: 
    @param tz: 
    """
    itervalUnit = getTimeframeIntervalUnit(timeframe)
    from_ts = datetime.timestamp(fromDatetime)
    to_ts = datetime.timestamp(toDatetime)
    
    yield fromDatetime
    
    while(True):
        from_ts+= itervalUnit*maxResultPageSize
        if(from_ts < to_ts):
            yield datetime.fromtimestamp(from_ts, tz= timezone(tz))
        else:
            yield toDatetime
            break
