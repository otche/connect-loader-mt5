#!/usr/bin/python3 
import sys
from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL
import logging
from  utilsBeforLog import getArgs, checkKey

LOG_FILE_PATH_KEY = "LOG_FILE_PATH"
LOG_LEVEL_KEY = "LOG_LEVEL"

class Logging():
    __logging = None

    def __init__(self): 
        """ Virtually private constructor. """
        if Logging.__logging!= None:
            raise Exception("This class is a singleton!")
        else:
            args = getArgs(sys.argv[1:])
            log_file_path = args[LOG_FILE_PATH_KEY] if (checkKey(LOG_FILE_PATH_KEY, args)) else "connect-loader-mt5.log"
            log_level = args[LOG_LEVEL_KEY] if (checkKey(LOG_LEVEL_KEY, args)) else "DEBUG"
    
            logging.basicConfig(
                filename=log_file_path,
                level=Logging.__switchLogLevel(log_level),
                format='%(levelname)s:[%(asctime)s]: %(message)s', 
                datefmt='%m/%d/%Y-%I:%M:%S-%p')

            Logging.__logging = logging

     
    @staticmethod 
    def getLogging():
        """ Static access method. """
        if Logging.__logging == None:
            Logging()
        return Logging.__logging

    
  
    @staticmethod 
    def __switchLogLevel(log_level_str):
        """ 
        @description:
        @param log_level_str: 
        """
        switcher = {
            "DEBUG": DEBUG,
            "INFO": INFO,
            "WARNING": WARNING,
            "ERROR": ERROR,
            "CRITICAL":  CRITICAL,
        }
        return switcher.get(log_level_str, DEBUG)
   

if __name__ == "__main__":
    Logging.getLogging()
    