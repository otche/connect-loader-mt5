#!/usr/bin/python3
import unittest
import sys
from utilsBeforLog import getArgs,checkKey, ArgEntryExeption, DuplicateEntryException
from utils import parseIntArg, objToDatatime, parseDatetimeStr, DatetimeStrFormatException, switch_TIMEFRAME,  getTimeframeIntervalUnit, buildRangePageIntervals, UndifinedTimeframe, DEFAULT_TIMEZONE
from datetime import datetime
from pytz import timezone
import MetaTrader5 as mt5root

class TestUtils(unittest.TestCase):
    """
    @describe:
    """
    def test_getArgs(self):
        """
        @describe:
        """
        args = getArgs( ['KEY1=VAL1', 'KEY2=VAL2', 'KEY3=VAL'])
        self.assertEqual(args['KEY1'], 'VAL1')
        self.assertEqual(args['KEY2'], 'VAL2')
        self.assertEqual(args['KEY3'], 'VAL')
        self.assertEqual(len(args), 3)

        with self.assertRaises(ArgEntryExeption) as expt:
            getArgs(['KEY1=VAL1=T', 'KEY2=VAL2'])
        self.assertEqual(str(expt.exception) ,str(ArgEntryExeption('KEY1=VAL1=T')))

        with self.assertRaises(DuplicateEntryException) as expt:
            getArgs(['KEY1=VAL1', 'KEY1=444'])
        self.assertEqual(str(expt.exception) ,str(DuplicateEntryException('KEY1')))
    

    def test_checkKey(self):
        """
        @describe:
        """
        args = getArgs( ['KEY1=VAL1', 'KEY2=VAL2', 'KEY3=VAL'])
        self.assertTrue(checkKey('KEY1', args))
        self.assertTrue(checkKey('KEY3', args))
        self.assertFalse(checkKey('KEY4', args))

    
    def test_parseIntArg(self):
        """
        @describe:
        """
        args = getArgs( ['KEY1=  5'])
        self.assertEqual(parseIntArg(args, 'KEY1'), 5)
        self.assertEqual(parseIntArg(args, 'KEY1',6), 5)
        self.assertEqual(parseIntArg(args, 'KEY2'), 0)
        self.assertEqual(parseIntArg(args, 'KEY2', 6), 6)

    
    def test_parseDatetimeStr(self):
        """
        @describe:
        """
        datetimeObj = parseDatetimeStr("2020/01/4")
        self.assertEqual(datetimeObj['year'], 2020)
        self.assertEqual(datetimeObj['month'], 1)
        self.assertEqual(datetimeObj['day'], 4)

        with self.assertRaises(DatetimeStrFormatException) as expt:
            parseDatetimeStr("2020")
        self.assertEqual(str(expt.exception) ,str(DatetimeStrFormatException('2020')))

        datetimeObj = parseDatetimeStr("2020/01/4|10:11:12")
        self.assertEqual(datetimeObj['year'], 2020)
        self.assertEqual(datetimeObj['month'], 1)
        self.assertEqual(datetimeObj['day'], 4)
        self.assertEqual(datetimeObj['hour'], 10)
        self.assertEqual(datetimeObj['minute'], 11)
        self.assertEqual(datetimeObj['second'], 12)

    def test_objToDatatime(self):
        """
        @describe:
        """
        dt = objToDatatime(parseDatetimeStr("2020/01/4|10:11:12"))
        local_datetime = datetime(year = 2020, month= 1, day= 4 , hour=10 , minute = 11, second = 12,  tzinfo=timezone("Etc/UTC"))
        self.assertEqual(str(dt), str(local_datetime))

    
    def test_switch_TIMEFRAME(self):
        """
        @describe:
        """
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_M1'), mt5root.TIMEFRAME_M1)
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_M3'), mt5root.TIMEFRAME_M3)
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_M10'), mt5root.TIMEFRAME_M10)
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_H1'), mt5root.TIMEFRAME_H1)
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_H4'), mt5root.TIMEFRAME_H4)
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_D1'), mt5root.TIMEFRAME_D1)
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_W1'), mt5root.TIMEFRAME_W1)
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_MN1'), mt5root.TIMEFRAME_MN1)
        self.assertEqual(switch_TIMEFRAME('TIMEFRAME_H14'), None)
        

    
    def test_getTimeframeIntervalUnit(self):
        """
        @describe:
        """
        self.assertEqual(getTimeframeIntervalUnit('TIMEFRAME_M1'), 60)
        self.assertEqual(getTimeframeIntervalUnit('TIMEFRAME_M4'), 60*4)
        self.assertEqual(getTimeframeIntervalUnit('TIMEFRAME_M30'), 60*30)
        self.assertEqual(getTimeframeIntervalUnit('TIMEFRAME_H4'), 60*60*4)
        self.assertEqual(getTimeframeIntervalUnit('TIMEFRAME_H8'), 60*60*8)
        self.assertEqual(getTimeframeIntervalUnit('TIMEFRAME_D1'), 60*60*24)
        self.assertEqual(getTimeframeIntervalUnit('TIMEFRAME_W1'), 60*60*24*7)
        self.assertEqual(getTimeframeIntervalUnit('TIMEFRAME_MN1'), 60*60*24*30)


        with self.assertRaises(UndifinedTimeframe) as expt:
            getTimeframeIntervalUnit('TIMEFRAME_D2')
        self.assertEqual(str(UndifinedTimeframe(timeframe = 'TIMEFRAME_D2')), str(expt.exception))
    

    def test_buildRangePageIntervals(self):
        """
        @describe:
        """
        fromDatetime = datetime(year=2020, month=1, day=8, hour=10, tzinfo= timezone(DEFAULT_TIMEZONE))
        toDatetime = datetime(year=2020, month=1, day=8, hour=13, tzinfo= timezone(DEFAULT_TIMEZONE))
        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize =10 , timeframe= "TIMEFRAME_M1",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)), 19)

        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize =30 , timeframe= "TIMEFRAME_M2",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)), 4)

        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize =4 , timeframe= "TIMEFRAME_M30",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)),3)

        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize =2 , timeframe= "TIMEFRAME_H1",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)),3)

        fromDatetime = datetime(year=2020, month=1, day=8, hour=15, tzinfo= timezone(DEFAULT_TIMEZONE))
        toDatetime = datetime(year=2020, month=1, day=20, hour=17, tzinfo= timezone(DEFAULT_TIMEZONE))

        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize =10 , timeframe= "TIMEFRAME_H1",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)),30)

        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize =10 , timeframe= "TIMEFRAME_H2",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)),16)

        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize = 3 , timeframe= "TIMEFRAME_H12",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)),10)

        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize = 2 , timeframe= "TIMEFRAME_D1",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)),8)

        fromDatetime = datetime(year=2019, month=1, day=8, tzinfo= timezone(DEFAULT_TIMEZONE))
        toDatetime = datetime(year=2020, month=2, day=20, tzinfo= timezone(DEFAULT_TIMEZONE))
        
        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize = 6 , timeframe= "TIMEFRAME_W1",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)),11)

        iter = buildRangePageIntervals(fromDatetime, toDatetime, maxResultPageSize = 4 , timeframe= "TIMEFRAME_MN1",tz= DEFAULT_TIMEZONE )
        self.assertEqual(len(list(iter)),5)



if __name__ == '__main__':

    unittest.main()